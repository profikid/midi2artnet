var Universe = require('./Universe')

var universes = [
    new Universe(1),
    new Universe(2, 7),
    new Universe(3),
    new Universe(4),
    new Universe(5),
    new Universe(6)
]

console.log(universes[0])
