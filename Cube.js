var Channel = require('./Channel')

module.exports = class Cube {
    constructor(id, startChannelIndex) {
        this.amountChannels = 30
        this.startChannelIndex = startChannelIndex
        this.id = id
        this.channels = []
        this.initChannels()
    }

    initChannels() {
        let endChannelIndex = this.startChannelIndex + this.amountChannels
        for (let index = this.startChannelIndex; index <  endChannelIndex; index++) {
            this.channels.push(new Channel(index, 240))
        }
    }
}
