var options = {
    host: 'localhost'
}

var artnet = require('artnet')(options);

module.exports = class Channels {
    constructor(id, value) {
        this.id = id
        this.value = value
        console.log('universe', this)
        this.send()
    }

    send() {
        artnet.set(this.id, this.value)
    }
}
